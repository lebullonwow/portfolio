import { Component, OnInit } from '@angular/core';
import { Resume } from 'src/app/data/resume';
import { Assets } from 'src/app/data/assets';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  constructor(
    public resume: Resume,
    public assets: Assets,
  ) { }

  ngOnInit() {
  }

  scroll(el: HTMLElement){
    el.scrollIntoView();
  }

}
