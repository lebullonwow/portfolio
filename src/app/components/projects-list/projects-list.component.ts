import { Component, OnInit } from '@angular/core';
import { Resume } from 'src/app/data/resume';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss']
})
export class ProjectsListComponent implements OnInit {

  constructor(
    public resume: Resume
  ) { }

  ngOnInit() {
  }

}
