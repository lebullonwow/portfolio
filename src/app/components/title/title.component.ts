import { Component, OnInit } from '@angular/core';
import { Resume } from 'src/app/data/resume';
import { Assets } from 'src/app/data/assets';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnInit {

  constructor(
    public resume: Resume,
    public assets: Assets,
  ) { }

  ngOnInit() {
  }

}
