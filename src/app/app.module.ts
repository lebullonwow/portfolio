import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ContentComponent } from './components/content/content.component';
import { SkillListComponent } from './components/skill-list/skill-list.component';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { BlurbComponent } from './shared/blurb/blurb.component';
import { ContactComponent } from './components/contact/contact.component';
import { TitleComponent } from './components/title/title.component';

import { AboutComponent } from './components/about/about.component';
import { ProjectComponent } from './shared/project/project.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ContentComponent,
    SkillListComponent,
    ProjectsListComponent,
    BlurbComponent,
    ContactComponent,
    TitleComponent,
    AboutComponent,
    ProjectComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
