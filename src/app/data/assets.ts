import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class Assets{
    public pride = "../assets/art-artistic-atmosphere-2027059.jpg?at=master";
    public comp = "../assets/comp.jpg";
    public jaguar = "../assets/jaguar.jpg";
    public bison = "../assets/bison.jpg";
    public norit = "../assets/norit.png";
    public distance = "../assets/distance.png";
    public symphony = "../assets/symphony.jpg";
    public sara = "../assets/sara.jpg";
    public tyler = "../assets/profpic.jpeg";
    public cgbs = "../assets/cgbs.png";
}