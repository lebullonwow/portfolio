import { Injectable } from '@angular/core';
import { Assets } from './assets';

let assets = new Assets();

@Injectable({
  providedIn: 'root',
})
export class Resume {

  public intro = `Frontend Developer, Full Stack Engineer`;

  public title = "Hey!  My name is Tyler.";

  public about = [
    "I was born and raised in Mississippi, and a piece of me will always call it home.",
    "I play french horn and a little bit of piano.",
    "I am a proud member and supporter of the LGBTQ+ community.",
    "I am a nerd at heart.",
  ];

  // public quote=`"We c an only see a short distance ahead, but we can see plenty there that needs to be done."
  // -Alan Turing`;

  public summary = [`
    A developer isn't good because of what they can do but how they can adapt.  
    My goal is to be flexible, a fast-learner, and a hard worker. I enjoy finding solutions to problems and getting my hands dirty to implement those solutions. I take pride in my work when I see it working in action.
  `,
  `I am a frontend developer at heart, and I am always working to grow and practice building applications that reach other people.  
  However, I always enjoy the challenge of stepping out of my comfort zone to learn other skills like backend development.  
  This attitude even extends to my hobbies, where I have learned skills such as hardware design as well as 3D modeling and printing.  
  And yes, I have combined all of this into individual projects to take the term "Full Stack Development" to its extremes.
  `,
  // Don’t be afraid to inject some personality
  // Add context to your career story

  //   During my education at Mississippi State University, I worked as a student assistant web developer.
    
  //   This included being responsible for designing and deploying an updated homepage for our department.

  //   Started in SAP Development and worked my way back toward web apps with SAP.

  // Brag about your accomplishments

  //   I have proven that I drive myself, regardless of where I am in the stack.

  // Utilize as much of the character limit as you can
  // Keep it readable with short paragraphs or bullet points
  // Don’t go overboard with special characters
  // Use a “call to action” at the end

  //   I encourage you to take a look at my portfolio to see my work in action.

  
  ];

  public vision = `
      Over the past few years, I have worked on backend and frontend.
      I would really like to take web dev seriously now.
      I consider myself to be a fullstack developer.
    `;

  public softskills = `
      I'm really good at tolerating people
    `;

  public education = {
    "university": "Mississippi State University",
    "school": "Bagley College of Engineering",
    "degreeType": "Bachelor of Science",
    "degree": "Computer Science",
    "year": "Class of 2014",
    "gpa": "3.4 GPA (Cum Laude)",
  };

  public beliefs = [
    "A strong community is better than a good technology... especially when it comes to frameworks",
    "Every tool, no matter how good it is, has downsides.",
    "{'s need to be on a new line.",
    "Dark Theme",
  ];

  public skills = [
    {
      name: "Languages",
      items: [
        "Python",
        "Javascript / NodeJS / Typescript",
        "HTML5 / CSS",
        "ABAP",
        "Java / C#",
      ]
    },
    {
      name: "Tools",
      items: [
        "Git",
        "Npm",
        "Grunt",
        "TFS / DevOps / Bitbucket",
        "Node Red"
      ]
    },
    {
      name: "Frameworks",
      items: [
        "Angular",
        "SAPUI5",
        "Django",
      ]
    },
    {
      name: "Practices",
      items: [
        "Continuous Integration",
        "Testing",
        "Scrum",
      ]
    },
    {
      name: "Craft",
      items: [
        "3D Printing",
        "Hardware Design",
        "Arduino",
        "Image Editing",
        "Audio Editing",
      ]
    }
  ];

  public projects = {
    work: [
      {
        //name: "Distance",
        description: "Rewrite of Distance Homepage",
        links: [
          {
            text: "Archive 2013", 
            link: "https://web.archive.org/web/20130302215501/http://www.distance.msstate.edu/"
          }
        ],
        
        summary: `
          In late 2012, I rewrote and published the homepage for Distance Learning at Mississippi State University.  
          This project was initially intended to be done by the fulltime developer, but that position was vacant during this time.
          However, I was able to take on the project, which included designing the page to be mobile, to conform to accessibility standards, and managed with a Content Management System.
          After putting some extra effort, I was able to drive the entire project and release the new page.
          Being responsible for the public face of our university's online program was a lot of pressure to be under as a student worker, but it has proven to me that I can manage my own work and deliver results.
          `,
        imageUrl: assets.distance,
        tags: [
          "Content Management",
          "HTML",
          "CSS",
        ]
      },
      {
        //name: "Ladi",
        description: "General PHP Backend for Web Apps",
        //url: "http://www.example.com",
        sourceUrl: "https://github.com/lebull/LADI/tree/master/LADI",
        summary: `In early Spring of 2013, the Center for Distance Education's Databases began as a coworker's class project. 
          The first database attempted was the marketing request database.  As summer came and the 
          project was turned in, the creator left for an internship and did not return. 
          I completely rewrote marketing using repeatable and predictable patterns.  Unfortunately, I would later
          learn that there are a multitude of frameworks for this exact reason.  However, I am
          still grateful for the experience of building these structures from the ground up.`,
        imageUrl: assets.distance,
        tags: [
          "HTML",
          "CSS",
          "JavaScript",
          "PHP",
          "SQL",
        ]
      },
      // {
      //   name: "CRM Mobile Launchpad",
      //   description: "PHP Backend for Internal sites developed for Distance Learning at Mississippi State University",
      //   //url: "http://www.example.com",
      //   sourceUrl: "",
      //   summary: ``,
      //   imageUrl: assets.distance,
      // },

      {
        description: "CRM Pipeline Applications",
        //name: "Opportunities",
        place: "International Paper",
        summary: `Between 2016 and 2018, I developed several SAP Fiori applications for our on-premise enterprise Customer Relationship Management systems.
          This included customization to support five separate businesses.  One business in particular had approximately 1000 users.  These apps are responsible for the majority of my experience in developing web applications in SAPUI5.`,
        tags: [
          "SAPUI5",
          "Fiori",
        ]
      },
      {
        description: "Fiori Mobile Launchpad Customization",
        //name: "Opportunities",
        place: "International Paper",
        summary: `In 2017, I was responsible for customizing and deploying an internal version of our SAP Fiori Mobile Launchpad app.  I also automated much of the process through the use of grunt tasks.
        Through this project, I gained experience in building iOS applications to be distributed across an enterprise.`,
        tags: [
          "Node",
          "IOS Build and Deployment",
          "Grunt",
        ],
      },
      /*{
        //name: "Casper",
        
        description: "Scrum Development Process Exploration and application rewrite",
        place: "International paper",
        url: "/",
        summary: `
          In late 2018, I was involved in a project to investigate using an agile development process in place of our team's traditional SAP development process. 
          The objective of this project was to 
          Took two shots at development, one with SAPUI5, the other with Angular.
          Included Test drivin development and Continuous Integration using Microsoft DevOps.
          Over 200 tests
        `,
        tags: [
          "Scrum",
          "Angular",
          "Material UI",
          "Test Driven Development",
          "Continuous Integration",
          "Netweaver Gateway",
        ],
      },*/
      {
        //name: "Portfolio",
        description: "Web-Based Resume and Portfolio",
        place: "You Are Here!",
        url: "/",
        sourceUrl: "https://bitbucket.org/lebullonwow/portfolio",
        summary: "This is a resume and portfolio webpage that has been implemented in Angular using the Bulma CSS library.",
        tags: [
          "Angular",
          "Bulma",
        ],
      }
    ],
    play: [
      {
        name: "CGBS",
        description: "College Game Board (of Science)",
        imageUrl: assets.cgbs,
        url: "https://cgbs.herokuapp.com/picker/season_1/user_1/",
        sourceUrl: "https://c9.io/lebull/cgbs",
        summary: `For the 2015 college football season, I decided to create a web app to track our winning picks among coworkers.  This became an opportunity to learn the DJango framework, and I was able to use feedback from users throughout the year.  The performance was... less than optimal.  I think this is mostly from my understanding of how the model in DJango works.  However, I was still able to improve the experience dramatically, even while we used the old fasion whiteboard the following year.
          If trying out the app, please be patient, it may take about 30 seconds to load some pages.`,
        tags: [
          "Web Development",
          "Python",
          "DJango",
          "Heroku",
        ]
      },
      {
        name: "Norit",
        description: "Ludum Dare 38",
        url: "https://ldjam.com/events/ludum-dare/38/norit",
        //sourceUrl: "https://bitbucket.org/lebullonwow/clara",
        summary: "In 2017, I participated in the 38th Ludum Dare game jam.  My goal was to not only assemble a game in this time, but also create as many assets and resources myself.  By the end of the week, I had supplied a game with all game art, sound effects, and music.",
        imageUrl: assets.norit,
        tags: [
          "Unity",
          "C#",
        ],
      },
      {
        name: "Bison",
        description: "Wireless Video Game Controller Extension",
        //url: "http://www.example.com",
        sourceUrl: "https://github.com/lebull/Bison",
        summary: "Bison is an input device that behaves as an extension to an x52 joystick, usually used for flight simulators.  The setup is completely wireless apart from power, as it uses a raspberry pi zero to send inputs to a vJoy instance on the host computer.",
        imageUrl: assets.bison,
        tags: [
          "Hardware Design",
          "Linux (Raspberry Pi)",
          "Python",
          "Networking",
          "3D Printing",
        ]
      },
      {
        name: "Jaguar",
        description: "IoT Garage Door",
        // url: "https://www.github.com/lebull/jaguar",
        //sourceUrl: "https://github.com/lebull/jaguar",
        summary: `I created IoT garage door opener that is driven by a wifi-enabled microcontroller.  It can tell if the garage door is open or closed by polling an ultrasonic rangefinder placed along the side of the top rail, and it can activate the door through a relay connected to the outside terminals of the opener.
            Thanks to a node red server, Alexa is also supported (I use this every day).
          `,
        imageUrl: assets.jaguar,
        tags: [
          "Hardware Design",
          "Microcontroller programming",
          "Networking",
          "Node-Red",
          "3D Printing",
        ]
      },
      {
        name: "Symphony",
        description: "Dynamic LED Backlight",
        //url: "http://www.example.com",
        sourceUrl: "https://github.com/lebull/Symphony",
        summary: "Symphony is a backlight to my living room entertainment center.  It consists of 8 segments of addressable RGB LEDs and is driven by an ESP8266.  By default, it presents a repeating rainbow pattern, but it will also accept a TCP connection to control the individual lights.  I also wrote a driver using PyAudio to visualize music placed on the host machine.",
        imageUrl: assets.symphony,
        tags: [
          "Hardware Design",
          "Microcontroller programming",
          "Networking",
          "Python",
          "Digital Signal Processing",
          "3D Printing",
        ]
      },


      // {
      //   name: "Clara",
      //   description: "Full-Stack Home IoT Implementation",
      //   url: "http://windyplace.ddns.net",
      //   sourceUrl: "https://bitbucket.org/lebullonwow/clara",
      //   summary: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vulputate odio ut enim blandit volutpat maecenas volutpat blandit. At tempor commodo ullamcorper a. Massa id neque aliquam vestibulum morbi. Vitae purus faucibus ornare suspendisse sed nisi lacus sed viverra. Volutpat blandit aliquam etiam erat velit. Gravida rutrum quisque non tellus orci ac. Elementum nibh tellus molestie nunc. Massa eget egestas purus viverra accumsan. Sollicitudin tempor id eu nisl nunc mi ipsum faucibus. Nulla at volutpat diam ut venenatis tellus in metus. Eget aliquet nibh praesent tristique magna.",
      //   tags: [
      //     "Angular",
      //     "Firebase",
      //     "MaterialUI",
      //     "Networking",
      //     "Security",
      //   ]
      // },
    ]
  };

      public contact = {
  name: "Tyler Darsey",
  email: "mailto:bulldogboy912000@yahoo.com",
  phone: "(601)-513-4027",
  addressStreet: "5832 Shiloah Cove South",
  addressPlace: "Olive Branch, MS 38654",
  github: "https://github.com/lebull",
  bitbucket: "https://bitbucket.com/lebullonwow",
  linkedin: "https://www.linkedin.com/in/tyler-darsey-10795b51/",
  facebook: "https://www.facebook.com/tyler.darsey",
};

      public workHistory = [
  {
    name: "International Paper",
    link: "https://www.internationalpaper.com/",
    time: "2014-Present",
    position: "SAP Developer",
    location: "Memphis, TN",
  },
  {
    name: "Center for Distance Education, Mississippi State University",
    link: "https://www.msstate.edu/",
    time: "2011-2014",
    position: "Assistant Web Developer",
    location: "Starkville, MS"
  }
]
}